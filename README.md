项目使用开源项目BootDo搭建。复用此开源项目在上面增加了人工智能的模块

 **

### 此项目不再更新，部分功能和人工智能模块已经全部迁移到https://gitee.com/xshuai/xai   项目** 

BootDo源码地址:https://gitee.com/lcg0124/bootdo

百度AI地址:https://ai.baidu.com

项目文档:https://x-ai.mydoc.io/

 **人脸检测参数修改** 

- 请修com\xai\baiduai\restapi\common\BDContants.java 中相关的参数

 **数据库文件** 

- 请在附件中自行下载

 **人脸检测网页截图:** 
![人脸检测网页截图](https://gitee.com/uploads/images/2018/0203/143236_e011c6fd_131538.jpeg "页面截图.jpg")
 **人脸检测数据截图:** 
![人脸检测数据截图](https://gitee.com/uploads/images/2018/0203/143257_aa347c5a_131538.jpeg "管理页面.jpg")

 **图像识别网页截图:**
![图像识别网页截图](https://gitee.com/uploads/images/2018/0209/110849_47956a67_131538.jpeg "图像识别.jpg")
 **图像识别数据截图:**
![植物识别](https://gitee.com/uploads/images/2018/0209/110911_c18932a4_131538.jpeg "植物识别.jpg")
 **文字识别网页截图** 
![通用文字识别](https://gitee.com/uploads/images/2018/0405/120259_8c5249b6_131538.png "通用文字识别.png")
